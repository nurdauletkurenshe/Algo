#include <bits/stdc++.h>
using namespace std;

typedef long long int ll;
typedef unsigned long long int ull;

const ll MAXL=9223372036854775806;
const int ARR=2*1000*100+256;
const int MAXI=2147483647;
const double pi=3.14159265359;

ll a[2*100*1000+256];
ll b[2*100*1000+256];
int main(){
    int n,m,ans=0;
    cin>>n>>m;
    while(1){
     if(n<m)swap(n,m);
     if(n-2<0||m-1<0)break;
     n-=2;
     m--;
     ans++;
    }
    cout<<ans;
}


